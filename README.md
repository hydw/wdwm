# wdwm

## patches used in dwm-6.4
- [dwm-actualfullscreen-20211013-cb3f58a.diff](https://dwm.suckless.org/patches/actualfullscreen/)
- [dwm-cfacts-vanitygaps-6.4_combo.diff](https://dwm.suckless.org/patches/vanitygaps/)
- [dwm-hide_vacant_tags-6.4.diff](https://dwm.suckless.org/patches/hide_vacant_tags/)
- [dwm-restartsig-20180523-6.2.diff](https://dwm.suckless.org/patches/restartsig/)
- [dwm-restoreafterrestart-20220709-d3f93c7.diff](https://dwm.suckless.org/patches/restoreafterrestart/)
- [dwm-statuscmd-20210405-67d76bd.diff](https://dwm.suckless.org/patches/statuscmd/)

## patches used in st-0.9
- [st-0.8.5-autocomplete-20220327-230120.diff](https://st.suckless.org/patches/autocomplete/)
- [st-anysize-20220718-baa9357.diff](https://st.suckless.org/patches/anysize/)
- [st-blinking_cursor-20230819-3a6d6d7.diff](https://st.suckless.org/patches/blinking_cursor/)
- [st-font2-0.8.5.diff](https://st.suckless.org/patches/font2/)
- [st-gruvbox-dark-0.8.5.diff](https://st.suckless.org/patches/gruvbox/)
- [st-scrollback-20210507-4536f46.diff, st-scrollback-mouse-20220127-2c5edf2.diff, st-scrollback-mouse-increment-0.8.2.diff](https://st.suckless.org/patches/scrollback/)
- [7672445bab01cb4e861651dc540566ac22e25812.diff](https://github.com/nimaipatel/st/blob/master/patches/7672445bab01cb4e861651dc540566ac22e25812.diff)
